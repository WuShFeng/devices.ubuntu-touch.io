import pMemoize from "p-memoize";

// Import data
import test from "./testData.js";
import elaborate from "./elaborateData.js";
import { mergeVariants, mergeReleases } from "./mergeData.js";

const getDevices = await import.meta.glob("/data/devices/*/data.md");
const getReleases = await import.meta.glob("/data/devices/*/releases/*.md");

async function getAll(memoCode) {
  // Get releases data from files
  let releases = await Promise.all(
    Object.values(getReleases).map(async (node) => {
      let release = await node();
      release.frontmatter.codename = release.file.split("/").at(-3);
      release.frontmatter.release = release.file
        .split("/")
        .pop()
        .split(".")
        .shift();
      release.frontmatter.content = await release.compiledContent();
      return release.frontmatter;
    })
  );

  // Get device data from files and add basic information
  let devices = await Promise.all(
    Object.values(getDevices).map(async (node) => {
      let device = await node();
      device.frontmatter.codename = device.file.split("/").at(-2);
      device.frontmatter.content = await device.compiledContent();
      device.frontmatter.filePath = device.file;
      return device.frontmatter;
    })
  );

  // Build variant and release overlay
  devices = mergeReleases(devices, releases);
  devices = mergeVariants(devices);

  // Get global device data from other sources
  return await Promise.all(
    devices.map(async (device) => {
      const testedPage = await test(
        "test-device:" + device.codename + "@" + device.release,
        device
      );
      const elaboratedPage = await elaborate(
        "elaborate-device:" + device.codename + "@" + device.release,
        testedPage
      );
      return elaboratedPage;
    })
  );
}

export default pMemoize(getAll);
