---
name: "Samsung Galaxy Tab A8 10.5 (2021, SM-X200/X205)"
deviceType: "tablet"

deviceInfo:
  - id: "cpu"
    value: "UNISOC Tiger T618"
  - id: "chipset"
    value: "8-cores chipset with 2x Cortex-A75 cores clocked at 2.0GHz and 6x Cortex-A55 cores clocked at 2.0GHz"
  - id: "gpu"
    value: "Mali G52 MP2"
  - id: "rom"
    value: "32/64/128 GB"
  - id: "ram"
    value: "2/3/4 GB"
  - id: "battery"
    value: "7040 mAh"
  - id: "display"
    value: '10.5" TFT LCD, 1200 x 1920 (216 PPI)'
  - id: "rearCamera"
    value: "Single 8MP, AF"
  - id: "frontCamera"
    value: "Single 5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "246.8mm x 161.9mm x 6.9mm"
  - id: "weight"
    value: "508g"
  - id: "releaseDate"
    value: "January 2022"

externalLinks:
  - name: "Telegram chat"
    link: "https://t.me/UT_on_GalaxyTabA8"
  - name: "Source repositories"
    link: "https://gitlab.com/ubports/porting/community-ports/android11/samsung-galaxy-tab-a8"
contributors:
  - name: "TheKit"
    forum: https://forums.ubports.com/user/thekit
---
